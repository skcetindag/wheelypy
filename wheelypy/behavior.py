import natsort


class Behaviour():
    def __init__(self, animalid, 
                dateinterval='200101', 
                criteria=None,
                load_behave=True,
                load_data=True, 
                autostart=True, 
                autoplot=True, 
                saveloc=None):

    def get_sessions(self):
        '''
        self.session_list   n x 2 matrix that has the sesion directory names in the 1st column
                            and session dates in datetime format in the 2nd column
        '''
        search_string_pyvstim = '{0}_wheel'.format(self.animalid)
        search_string_stimpy = '{0}__no_cam'.format(self.animalid)
        session_list = []
        for s in os.listdir(self.presentationfolder):
            if search_string_stimpy in s or search_string_pyvstim in s:
                session_list.append(s)

        session_list = natsort.natsorted(session_list, reverse=False)

        date_list = [dt.strptime(x.split('_')[0], '%y%m%d') for x in session_list]

        self.session_list = [[session_list[i], date] for i, date in enumerate(date_list)]

    # for now only thresholds bigger values
    #TODO: FIX ISSUE WITH KEY MISSING!
    def implement_criteria(self, rowtoappend):
        # filter data according to given criteria
        if self.criteria is None:
            display('No criteria given, including all {0} sessions'.format(len(self.session_list)))
            return 1
        else:
            satisfied = []
            for k in self.criteria.keys():
                if k not in rowtoappend.keys():
                    display('Criteria {0} not found in data, skipping'.format(k))
                    continue
                else:
                    if rowtoappend[k] >= self.criteria[k]:
                        satisfied.append(1)
                    else:
                        display('{0} not satisfied: {1} < {2}'.format(k, rowtoappend[k], self.criteria[k]))
                        satisfied.append(0)
            if 0 in satisfied:
                return 0
            else:
                return 1

    def filter_dates(self,df):
        '''
        Filters the behavior data to analyze only the data in the date range
        '''
        # dateinterval is a list of two date strings e.g. ['200127','200131']
        if type(self.dateinterval) is not list:
            self.dateinterval = [self.dateinterval]
            # add current day as end date
            self.dateinterval.append(dt.today().strftime('%y%m%d'))

        if len(self.dateinterval) == 2:
            startdate = dt.strptime(self.dateinterval[0], '%y%m%d')
            enddate = dt.strptime(self.dateinterval[1], '%y%m%d')
        else: 
            raise Exception('Date interval wrong!')

        display('Analyzing between {0} - {1}'.format(self.session_list[0][1],self.session_list[-1][1]))

        return df[(df['session_date'] >= startdate) & (df['session_date'] <= enddate)]

