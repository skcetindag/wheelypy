import numpy as np
import pandas as pd
import os
import json
import sys
import ast

from datetime import datetime as dt
try:
    from cStringIO import StringIO
except:
    try:
        from StringIO import StringIO
    except ImportError:
        from io import StringIO

def display(msg):
    sys.stdout.write('['+dt.today().strftime('%y-%m-%d %H:%M:%S')+'] - ' + msg + '\n')
    sys.stdout.flush()

def get_turning_points(x):
    N = 0
    for i in range(1, len(x) - 1):
        if x[i - 1] < x[i] and x[i + 1] < x[i]:
            N += 1
        elif x[i - 1] > x[i] and x[i + 1] > x[i]:
            N += 1
    return N

def find_nearest(array, value):
    if isinstance(array,list):
        array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return [idx, array[idx]]

def find_duplicates(array):
    seen = {}
    duplicates = []
    array = np.asarray(array)
    if len(array) == len(set(array)):
        return None
    else:
        for x in array:
            if x not in seen:
                seen[x] = 1
            else:
                duplicates.append(x)
        return duplicates

def median_threshold(array):
    #is this adaptive thresholding?
    #can also get a Pandas.Series input:
    thresh = 0
    for i in array:
        if len(i):
            thresh += np.median(i)
    thresh = thresh / len(self.session_data)
    return thresh

def parsePref(preffile):
    with open(preffile, 'r') as infile:
        pref = json.load(infile)
        return pref

def parseVStimLog(fname):
    comments = []
    faulty = True
    with open(fname,'r') as fd:
        for line in fd:
            if line.startswith('#'):
                comments.append(line.strip('\n').strip('\r'))
                if '# CODES: stateMachine=20' in line:
                    faulty = False

    # if state machine init not present
    if faulty:
        display('LOGGING INITIALIZATION FAULTY, FIXING COMMENT HEADERS')
        comments += ['# Started state machine v1.2 - timing sync to rig',
        '# CODES: stateMachine=20',
        '# STATE HEADER: code,elapsed,cycle,newState,oldState,stateElapsed,trialType',
        '# CODES: vstim=10',
        '# VLOG HEADER:code,presentTime,iStim,iTrial,iFrame,blank,contrast,posx,posy,indicatorFlag']

    codes = {}
    vlogheader = []
    righeader = []
    for c in comments:
        if c.startswith('# CODES:'):
            cod = c.strip('# CODES:').strip(' ').split(',')
            for cd in cod:
                k,v = cd.split('=')
                codes[int(v)] = k
        elif c.startswith('# VLOG HEADER:'):
            cod = c.strip('# VLOG HEADER:').strip(' ').split(',')
            vlogheader = [c.replace(' ','') for c in cod]
        elif c.startswith('# RIG CSV:'):
            cod = c.strip('# RIG CSV:').strip(' ').split(',')
            righeader = [c.replace(' ','') for c in cod]

    logdata = pd.read_csv(fname,
                          names = [i for i in range(len(vlogheader))],
                          delimiter=',',
                          header=None,comment='#',engine='c')
    
    data = dict()
    for v in codes.keys():
        k = codes[v]
        data[k] = logdata[logdata[0]==v]
        if len(data[k]):
            
            #get the columns from most filled row
            tmp_nona = data[k].dropna()
            if len(tmp_nona):
                tmp = tmp_nona.iloc[0].copy()
            else:
                tmp = data[k].iloc[0].copy()
            ii = np.where([type(t) is str for t in tmp])
            for i in ii:
                tmp[i] = 0
                
            idx = np.where([~np.isnan(d) for d in tmp])[0]
            data[k] = data[k][idx]
            if len(idx) <= len(righeader):
                cols = righeader
            else:
                cols = vlogheader[:len(idx)]
            data[k] = pd.DataFrame(data = data[k])
            data[k].columns = cols
            
    if 'vstim' in data.keys() and 'screen' in data.keys():
        # extrapolate duinotime from screen indicator
        indkey = 'not found'
        fliploc = []
        if 'indicatorFlag' in data['vstim'].keys():
            indkey = 'indicatorFlag'
            fliploc = np.where(np.diff(np.hstack([0,
                                                  data['vstim']['indicatorFlag'],
                                                  0]))!=0)[0]
        elif 'blank' in data['vstim'].keys():
            indkey = 'blank'
            fliploc = np.where(np.diff(np.hstack([0,
                                                  data['vstim']['blank']==0,
                                                  0]))!=0)[0]
        if len(data['screen'])==len(fliploc):
            data['vstim']['duinotime'] = interp1d(
                fliploc,
                data['screen']['duinotime'],
                fill_value="extrapolate")(
                    np.arange(len(data['vstim'])))
        else:
            
            print(
                'The number of screen pulses {0} does not match the visual stimulation {1}:{2} log.'
                  .format(len(data['screen']),indkey,len(fliploc)))
    return data,comments

# TODO: Data from stimlog and riglog now has to be combined downstrem
def parseStimpyLog(fname):
    """ Parses the log file (riglog or stimlog) and returns data and comments

        :return: data and comments 
        :rtype: DataFrame and list
    """
    comments = []
    faulty = False
    with open(fname,'r') as fd:
        for i,line in enumerate(fd):
            if line.startswith('#'):
                comments.append(line.strip('\n').strip('\r'))
                if '# CODES: stateMachine=20' in line:
                    faulty = False
                    
    # if state machine initialization not present directly add the state machine comment lines to comments list
    if faulty:
        display('LOGGING INITIALIZATION FAULTY, FIXING COMMENT HEADERS')
        toAdd = ['# Started state machine v1.2 - timing sync to rig',
        '# CODES: stateMachine=20','# STATE HEADER: code,elapsed,cycle,newState,oldState,stateElapsed,trialType',
        '# CODES: vstim=10',
        '# VLOG HEADER:code,presentTime,iStim,iTrial,iFrame,blank,contrast,posx,posy,indicatorFlag']
        comments += toAdd
    
    codes = {}
    for c in comments:
        if c.startswith('# CODES:'):
            code_list = c.strip('# CODES:').strip(' ').split(',')
            for code_str in code_list:
                code_name,code_nr = code_str.split('=')
                codes[int(code_nr)] = code_name
        elif c.startswith('# VLOG HEADER:'):
            header_list = c.strip('# VLOG HEADER:').strip(' ').split(',')
            vlogheader = [header_str.replace(' ','') for header_str in header_list]
        elif c.startswith('# STATE HEADER:'):
            header_list = c.strip('# STATE HEADER:').strip(' ').split(',')
            stateheader = [header_str.replace(' ','') for header_str in header_list]
        elif c.startswith('# RIG CSV:'):
            header_list = c.strip('# RIG CSV:').strip(' ').split(',')
            righeader = [header_str.replace(' ','') for header_str in header_list]
              
    if fname.endswith('.riglog'):
        display('Parsing riglog...')
        header = righeader
    elif fname.endswith('.stimlog'):
        display('Parsing stimlog...')
        header = vlogheader
    
    logdata = pd.read_csv(fname,
                      names = [i for i in range(len(header))],
                      delimiter=',',
                      header=None,comment='#',engine='c')
    
    if fname.endswith('.riglog'):
        logdata = logdata.applymap(remove_brackets)
        
    data = dict()
    for code_nr in codes.keys():
        code_key = codes[code_nr]
        data[code_key] = logdata[logdata[0] == code_nr]
        if len(data[code_key]):
            # get the column amount from most filled row
            tmp_nona = data[code_key].dropna()
            if len(tmp_nona):
                tmp = tmp_nona.iloc[0].copy()
            else:
                tmp = data[code_key].iloc[0].copy()
            """
            TODO: This is semi hard coded, 
            maybe find a better way to automate this 
            so it is easier to add different loggers and their dedicated headers
            """
            if code_nr==20:
                state_data = data[code_key].loc[:,0:len(stateheader)-1]
                state_data.columns = stateheader
                data[code_key] = state_data
            else:
                data[code_key].columns = header
        else:
            display('No data found for log key : {0}'.format(code_key))
            
    return data,comments

def extrapolate_time(data):
    """ Extrapolates duinotime from screen indicator

        :param data: 
        :type data: dict
    """
    if 'vstim' in data.keys() and 'screen' in data.keys():
        
        indkey = 'not found'
        fliploc = []
        if 'indicatorFlag' in data['vstim'].keys():
            indkey = 'indicatorFlag'
            fliploc = np.where(np.diff(np.hstack([0,
                                                  data['vstim']['indicatorFlag'],
                                                  0]))!=0)[0]
        elif 'photo' in data['vstim'].keys():
            indkey = 'photo'
            fliploc = np.where(np.diff(np.hstack([0,
                                                  data['vstim']['photo']==0,
                                                  0]))!=0)[0]
        if len(data['screen'])==len(fliploc):
            data['vstim']['duinotime'] = interp1d(
                fliploc,
                data['screen']['duinotime'],
                fill_value="extrapolate")(
                    np.arange(len(data['vstim'])))
        else:
            
            print(
                'The number of screen pulses {0} does not match the visual stimulation {1}:{2} log.'
                  .format(len(data['screen']),indkey,len(fliploc)))
    return data

def remove_brackets(x,convert_flag=True):
    """ Removes brackets form a read csv file, best applied in a dataframe with df.applymap(remove_brackets)

        :param x: value to have the brackets removed(if exists)
        :type x: str, int, float
        :param convert_flag: Flag to determine whether to convert to float or not(this should be True 99% of time)
        :type convert_flag: bool
        :return: returns the bracket removed value
        :rtype: float
    """
    if isinstance(x,str):
        if '[' in x:
            x = x.strip('[')
        elif ']' in x:
            x = x.strip(']')
        if convert_flag:
            try:
                return float(x)
            except:
                return '' 
        else:
            return x
    else:
        return x

def parseProtocolFile(protfile):
    options = {}
    with open(protfile,'r') as fid:
        string = fid.read().split('\n')
        for i,s in enumerate(string):
            tmp = s.split('=')
            tmp = [t.strip(' ') for t in tmp]
            # Because the first lines are always like this...
            if len(tmp)>1:
                options[tmp[0]] = tmp[1].replace('\r','')
                try:
                    prot[tmp[0]] = int(prot[tmp[0]])
                except:
                    try:
                        prot[tmp[0]] = float(prot[tmp[0]])
                    except:
                        pass
            else:
                break
        tmp = string[i::]
        tmp = [t.replace('\r','').replace('\t',' ').strip().split() for t in tmp]
        tmp = [','.join(t) for t in tmp ]
        try:
            params = pd.read_csv(StringIO(u"\n".join(tmp)),
                                 index_col=None)
        except pd.io.common.EmptyDataError:
            params = None
    return options,params
