import numpy.ma as ma
import matplotlib.patheffects as path_effects
from scipy.optimize import curve_fit
from scipy.stats import sem
from wheelypy.fit_funcs import *


class WheelCurve:
    def __init__(self, name, rawdata, model='erf_psycho2', nfits=5, resolution = 100):
        '''
        Calculates the psychometric curve for nonopto and opto(if there is) trials
            name:       label for calculated trajectory
            rawdata:    dataframe slice from session_data
            model:      the distribution to be fitted to the data
            nfits:      number of fitting iterations
            resolution: resolution of points in the fitted curve
        '''
        self.name = name
        self.rawdata = rawdata

        self.proportion_right = []
        self.proportion_left = []
        self.binomial_right = []
        self.binomial_left = []
        self.counts = []

        self.model = model
        self.nfits = nfits
        self.resolution = resolution

        self.contrast = np.unique(self.rawdata['contrast'])
        neg_contrast = [-1 * c for c in self.contrast]
        self.contrast_x = np.unique(np.hstack((neg_contrast[::-1], self.contrast)))

        self.calculate_percentages()
        # self.fit_curve()
        self.fit_curve_mle()


    def calculate_percentages(self):
        '''
        Calculates the biases for different contrats values with %95 binomial confidence
        '''
        for i, c in enumerate(self.contrast):
            # stim on left
            # naming convention x_right_left means rightward answers in left stim side
            contrast_left_df = self.rawdata[(self.rawdata['contrast'] == c) & (self.rawdata['stim_side'] == -1)] 
            if len(contrast_left_df):
                incorrectly_right = contrast_left_df[contrast_left_df['answer'] == -1]
                percent_right_left = len(incorrectly_right) / len(contrast_left_df)
                bino_right_left = 1.96 * np.sqrt(
                    (percent_right_left * (1 - percent_right_left)) / len(contrast_left_df))  # 95% binomial

                correctly_left = contrast_left_df[contrast_left_df['answer'] == 1]
                percent_left_left = len(correctly_left) / len(contrast_left_df)
                bino_left_left = 1.96 * np.sqrt(
                    (percent_left_left * (1 - percent_left_left)) / len(contrast_left_df))  # 95% binomial
            else:
                incorrectly_right = []
                correctly_left = []
                percent_right_left = 0
                percent_left_left = 0
                bino_right_left = 0
                bino_left_left = 0
            
            # stim on right
            contrast_right_df = self.rawdata[(self.rawdata['contrast'] == c) & (self.rawdata['stim_side'] == 1)]
            if len(contrast_right_df):
                correctly_right = contrast_right_df[contrast_right_df['answer'] == 1]
                percent_right_right = len(correctly_right) / len(contrast_right_df)
                bino_right_right = 1.96 * np.sqrt(
                    (percent_right_right * (1 - percent_right_right)) / len(contrast_right_df))  # 95% binomial

                incorrectly_left = contrast_right_df[contrast_right_df['answer'] == -1]
                percent_left_right = len(incorrectly_left) / len(contrast_right_df)
                bino_left_right = 1.96 * np.sqrt(
                    (percent_left_right * (1 - percent_left_right)) / len(contrast_right_df))  # 95% binomial
            else:
                correctly_right = []
                incorrectly_left = []
                percent_right_right = 0
                percent_left_right = 0
                bino_right_right = 0
                bino_left_right = 0

            if c == 0:
                contrast_zero = self.rawdata[self.rawdata['contrast'] == c]
                percent_zero_right = (len(incorrectly_right) + len(correctly_right)) / len(contrast_zero)
                bino_zero_right = 1.96 * np.sqrt(
                    (percent_zero_right * (1 - percent_zero_right)) / len(contrast_zero))  # 95% binomial

                percent_zero_left = (len(incorrectly_left) + len(correctly_left)) / len(contrast_zero)
                bino_zero_left = 1.96 * np.sqrt(
                    (percent_zero_left * (1 - percent_zero_left)) / len(contrast_zero))  # 95% binomial

                self.proportion_right.append(percent_zero_right)
                self.binomial_right.append(bino_zero_right)

                self.proportion_left.append(percent_zero_left)
                self.binomial_left.append(bino_zero_left)

                self.counts.append(len(contrast_zero))
            else:
                # this is to fill the lists from both sides
                # left side
                self.proportion_right.insert(0, percent_right_left)
                self.binomial_right.insert(0, bino_right_left)

                self.proportion_left.insert(0, percent_left_left)
                self.binomial_left.insert(0, bino_left_left)

                self.counts.insert(0,len(contrast_left_df))
                #right side
                self.proportion_right.append(percent_right_right)
                self.binomial_right.append(bino_right_right)

                self.proportion_left.append(percent_left_right)
                self.binomial_left.append(bino_left_right)

                self.counts.append(len(contrast_right_df))

        self.proprtion_right = np.asarray(self.proportion_right)
        self.proportion_left = np.asarray(self.proportion_left)
        self.binomial_right = np.asarray(self.binomial_right)
        self.binomial_left = np.asarray(self.binomial_left)
        self.counts = np.asarray(self.counts)

    def fit_curve(self):
        '''
        x0 = x value of the sigmoid midpoint
        L = max value of sigmoid
        k = logistic growth rate aka steepness
        b = y-offset
        '''
        sigmoid = lambda x, x0, L, k, b: L / (1.0 + np.exp(-k * (x-x0))) + b
        #parameter bounds
        bounds=([np.min(self.contrast_x),-np.inf,-np.inf,np.min(self.proportion_right)],
                [np.max(self.contrast_x),np.inf,np.inf,np.inf])
        try:
            popt, pcov = curve_fit(sigmoid,
                                   self.contrast_x,
                                   self.proportion_right,
                                   method='dogbox',
                                   bounds=bounds)
            self.contrast_x_fitted = np.linspace(self.contrast_x.min(), self.contrast_x.max(), self.resolution).reshape(-1, 1)
            self.proportion_right_fitted = np.asarray(sigmoid(self.contrast_x_fitted, *popt)).reshape(-1, 1)
        
        except:
            # If can't fit just put out the same data as original
            print('Error fitting the data')
            self.contrast_x_fitted = self.contrast_x
            self.proportion_right_fitted = self.proportion_right

    def fit_curve_mle(self):
        # data structure is [signed_contrasts, trial_count_per contrast, proportion_right, proportion_left]
        data = np.vstack((self.contrast_x, self.counts, self.proportion_right))
        self.pars, self.L = mle_fit(data, self.model, nfits=self.nfits)

        self.contrast_x_fitted = np.linspace(self.contrast_x.min(), self.contrast_x.max(), self.resolution).reshape(-1, 1)
        self.proportion_right_fitted = np.asarray(erf_psycho2(self.pars,self.contrast_x_fitted)).reshape(-1, 1)

    # def plot_psychometric(self,**kwargs):
    #     pass


class WheelTrajectory:
    def __init__(self, name, rawdata):
        '''
        Calculates the wheel trajectories for different stim sides and different contrast values
            name:    label for calculated trajectory
            rawdata: dataframe slice from session_data
        '''
        self.name = name
        self.rawdata = rawdata
        self.trajectories = {}
        self.averages = {}
        self.sems = {}
        if np.unique(self.rawdata['opto']).tolist() == [0]:
            self.optogenetic = False
        else:
            self.optogenetic = True
        # apply len to wheel column
        self.rawdata.loc[:,'wheel_len'] = self.rawdata['wheel'].apply(lambda x: len(x))

        self.rawdata.loc[:,'wheel_synced'] = self.rawdata['wheel'].apply(self.sync)
        self.rawdata.reset_index(drop=True, inplace=True)

        self.get_trajectories()

    def sync(self,x): 
        # sync timeframes to start from 0
        # make wheel positions leveled to 0
        x = np.asarray(x)
        if len(x):
            time_synced = np.add(x[:,0], -1*x[0,0]).reshape(-1,1).tolist()
            pos_synced = np.add(x[:,1],-1*x[0,1]).reshape(-1,1).tolist()
            
            synced = np.hstack((time_synced,pos_synced))
            return synced
        else:
            pass
                
    def get_trajectories(self):
        contrast_list = np.unique(self.rawdata['contrast'])

        for c in contrast_list:
            self.trajectories[c] = {}
            self.averages[c] = {}
            self.sems[c] = {} #sem is standart error mean
            if self.optogenetic:
                opto_loop = [0,1]
            else:
                opto_loop = [0]
            for opto in opto_loop:
                c_slice = self.rawdata[(self.rawdata['contrast'] == c) & (self.rawdata['opto'] == opto)]

                c_slice.reset_index(drop=True, inplace=True)
                if len(c_slice):
                    imax = c_slice['wheel_len'].idxmax()
                    max_wheel = c_slice['wheel_synced'].iloc[imax]

                    # create the template masked array and mask
                    # rows = wheel data points
                    # columns = [time, wheel]
                    # depth = each trial that satisfies opto=o, contrast=c, stim_side=s, answer=answered
                    tmp = np.zeros((max_wheel.shape[0], max_wheel.shape[1], len(c_slice)))
                    mask = np.ones_like(tmp)

                    for i, row in c_slice.iterrows():
                        wheel_len = row['wheel_len']
                        wheel_vals = row['wheel_synced']
                        tmp[0:wheel_len, :, i] = wheel_vals
                        mask[0:wheel_len, :, i] = 0

                    # add it to the trajectory matrix
                    trj_array = ma.array(data=tmp, mask=mask)
                    
                    #get the mean 
                    avg = np.average(trj_array[:, 1, :], axis=1).data.reshape(-1, 1)
                    avg_t = max_wheel[:, 0].reshape(-1, 1)
                    avg_array = np.hstack((avg_t, avg))
                    
                    #get the s.e.m
                    sems_array = sem(trj_array[:,1,:], axis=1).reshape(-1,1)
                    
                else:
                    trj_array = None
                    avg_array = None
                    sems_array = None

                if opto == 1:
                    self.trajectories[c]['opto'] = trj_array
                    self.averages[c]['opto'] = avg_array
                    self.sems[c]['opto'] = sems_array
                else:
                    self.trajectories[c]['nonopto'] = trj_array
                    self.averages[c]['nonopto'] = avg_array
                    self.sems[c]['nonopto'] = sems_array

    # CURRENTLY ONLY FOR CORRECT AND INCORRECT ANSWERS
    def get_confidence(self):
        legit_answer_df = self.session_data[self.session_data['answer'] != 0]
        confidence_dict = {}
        for i, row in legit_answer_df.iterrows():
            nturns = get_turning_points(row['stim_pos'])
            confidence_index = 1 - (nturns / len(row['stim_pos']))
            confidence_dict[row['trial_no']] = [row['contrast'], confidence_index]

        return confidence_dict
