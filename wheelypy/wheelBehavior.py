import natsort
from wheelypy.wheelExperiment import *
from wheelypy.gsheet_functions import *
from wheelypy.plotters import *

class WheelBehavior():
    def __init__(self, animalid, 
                dateinterval='200101', 
                criteria=None,
                load_behave=True,
                load_data=True, 
                autostart=True, 
                autoplot=True, 
                saveloc=None):
        '''
        Analyzes the training progression of animals through multiple sessions
            animalid:     id of the animal to be analyzed(e.g. KC033)
            dateinterval: interval of dates to analyze the data, 
                          can be a list of two dates, or a string of starting date
                          (e.g. ['200110','200619'] OR '200110')
            criteria:     criteria to include/exclude sessions. It's a dictionary with preset keys
                          (e.g. {answered_trials:0, answered_correct:0}
            load_behave:  flag to either load previously analyzed behavior data or to analyze it from scratch
            load_data:    flag to either load previously parsed data or to parse it again
            autostart:    flag to autostart
            autoplot:     flag to automatically plot available plots
            saveloc:      path to manually save the behavior data, 
                          normally saves it to the last session directory in the analysis folder  
        '''
        self.animalid = animalid
        self.presentationfolder = 'J:/data/presentation'
        self.analysisfolder = 'J:/data/analysis'
        self.dateinterval = dateinterval
        self.criteria = criteria
        self.load_behave = load_behave
        self.load_data = load_data
        # no point in reading saved behavior data if reanalyzing session data
        if not self.load_data:
            self.load_behave=False
        self.saveloc = saveloc
        self.autoplot = autoplot

        # get all of the session names of a given animalid
        self.get_sessions()
        if autostart:
            self.compile_data()
            self.pool_data()
            if autoplot:
                self.init_plotter(savefig=True)
                self.plot('psychometric')
                self.plot('performance')
                self.plot('health')
        
    def filter_dates(self,df):
        '''
        Filters the behavior data to analyze only the data in the date range
        '''
        # dateinterval is a list of two date strings e.g. ['200127','200131']
        if type(self.dateinterval) is not list:
            self.dateinterval = [self.dateinterval]
            # add current day as end date
            self.dateinterval.append(dt.today().strftime('%y%m%d'))

        if len(self.dateinterval) == 2:
            startdate = dt.strptime(self.dateinterval[0], '%y%m%d')
            enddate = dt.strptime(self.dateinterval[1], '%y%m%d')
        else: 
            raise Exception('Date interval wrong!')

        display('Analyzing between {0} - {1}'.format(self.session_list[0][1],self.session_list[-1][1]))

        return df[(df['session_date'] >= startdate) & (df['session_date'] <= enddate)]

    def get_sessions(self):
        '''
        self.session_list   n x 2 matrix that has the sesion directory names in the 1st column
                            and session dates in datetime format in the 2nd column
        '''
        search_string_pyvstim = '{0}_wheel'.format(self.animalid)
        search_string_stimpy = '{0}__no_cam'.format(self.animalid)
        session_list = []
        for s in os.listdir(self.presentationfolder):
            if search_string_stimpy in s or search_string_pyvstim in s:
                session_list.append(s)

        session_list = natsort.natsorted(session_list, reverse=False)

        date_list = [dt.strptime(x.split('_')[0], '%y%m%d') for x in session_list]

        self.session_list = [[session_list[i], date] for i, date in enumerate(date_list)]

    # for now only thresholds bigger values
    #TODO: FIX ISSUE WITH KEY MISSING!
    def implement_criteria(self, rowtoappend):
        # filter data according to given criteria
        if self.criteria is None:
            display('No criteria given, including all {0} sessions'.format(len(self.session_list)))
            return 1
        else:
            satisfied = []
            for k in self.criteria.keys():
                if k not in rowtoappend.keys():
                    display('Criteria {0} not found in data, skipping'.format(k))
                    continue
                else:
                    if rowtoappend[k] >= self.criteria[k]:
                        satisfied.append(1)
                    else:
                        display('{0} not satisfied: {1} < {2}'.format(k, rowtoappend[k], self.criteria[k]))
                        satisfied.append(0)
            if 0 in satisfied:
                return 0
            else:
                return 1

    def get_weight_water(self,date):
        animal_stats = {}

        self.glog_df.dropna(inplace=True)
        #current date data
        row = self.glog_df[(self.glog_df['Date']==date) & (self.glog_df['MouseID']==self.animalid)]
        if len(row):
            animal_stats['thresh_weight'] = row['Thresh'].values[0]
            animal_stats['weight'] = row['Weight'].values[0]
            animal_stats['extra_water'] = row['waterGiven'].values[0] 
        else:
            display('No Training Log entry {0} {1}'.format(self.animalid, date))
            animal_stats = {}

        return animal_stats

    def compile_data(self):

        logsheet = GSheet('Training Log')
        #Enter the animal id to sheet
        # curr_animal = logsheet.read_cell('K6')
        # if curr_animal != self.animalid:
        #     w = logsheet.write_cell('K6',self.animalid)
        #     if not w:
        #         raise Exception('Writing in Google Sheet unsuccesful!')

        self.glog_df = logsheet.read_sheet(0)
        
        behavior_data = pd.DataFrame()
        
        if self.load_behave:
            missing_sessions = []
            for i,_ in enumerate(self.session_list,1):
                # iterating from reverse
                sesh = self.session_list[-i]
                #check if behavior data exists in one of the session directories inside the analysis directory
                datapath = pjoin(self.analysisfolder,sesh[0],'trainingData.eel').replace("\\","/")
                if not os.path.exists(datapath):
                    missing_sessions.insert(0,sesh)
                else:
                    display('Found data in {0}, loading'.format(datapath))
                    behavior_data = pd.read_pickle(datapath)
                    last_saved_path = datapath
                    display('Adding {0} missing sessions to last analysis data'.format(len(missing_sessions)))
                    break
        else:
            missing_sessions = self.session_list

        for i,sesh in enumerate(missing_sessions):
            # gets the session summary from the analysis directory
            display('Analyzing... {0} / {1}'.format(i+1,len(missing_sessions)))
            self.current_sesh = sesh
            temp_session = WheelExperiment(sesh[0], load_flag=self.load_data, autoplot=self.autoplot)
            temp_summary = temp_session.session_summary
            session_row = {}
            if temp_summary is not None:
                session_row['session'] = temp_session
                session_row['session_no'] = len(behavior_data) + 1
                session_row['session_date'] = missing_sessions[i][1]
                session_row['rig'] = temp_session.session_meta['rig']
                session_row['level'] = temp_summary['level']

                animal_stats = self.get_weight_water(missing_sessions[i][1])
                session_row = {**session_row,**animal_stats}

                # append if satisfies criteria
                if self.implement_criteria(session_row):
                    behavior_data = behavior_data.append(session_row, ignore_index=True)
                else:
                    continue
            else:
                display('No data for this session, skipping...')
                continue
        
        # Failsafe date sorting for non-analyzed all trials and empty sessions(?)
        behavior_data = behavior_data.sort_values('session_date', ascending=True)
        behavior_data.dropna(inplace=True)
        behavior_data.reset_index(inplace=True,drop=True)

        self.behavior_data = self.filter_dates(behavior_data)

        
        # save behavior data to the last session analysis folder
        if self.saveloc is None:
            savepath = pjoin(self.analysisfolder,self.session_list[-1][0],'trainingData.eel').replace("\\","/")
        else:
            savepath = pjoin(self.saveloc,'{0}_{1}_trainingData.eel'.format(self.dateinterval[0],self.animalid)).replace("\\","/")
        
        self.behavior_data.to_pickle(savepath)
        display('Behavior data saved in {0}'.format(savepath))
        if self.load_behave and len(missing_sessions):
            display('Deleting the old data in {0}'.format(last_saved_path))
            # this is an extra security step, this should not happen
            if last_saved_path != self.session_list[-1][0]:
                os.remove(last_saved_path)
        return self.behavior_data
        
    # TODO: ADD LEVELS FOR TRIALS IF DYNAMIC LEVELING IS APPLIED
    def pool_data(self,pool_depth=5):
        self.pooled_curves = {}
        # this should also seperate opto
        levels = np.unique(self.behavior_data['level'])
        for l in levels:
            # a dictionary with stim types as keys and concatanated raw data as values
            self.pooled_curves[l] = {}
            tmp_dict = {}
            tmp_cnts = {}
            level_data = self.behavior_data[self.behavior_data['level']==l]

            # iterate from the bottom, last session first
            pool_cnt = 0
            for i,row in level_data[::-1].iterrows():
                pool_cnt += 1
                for k in row['session'].session_summary['curves'].keys(): 
                    if k not in tmp_dict.keys():
                        tmp_dict[k] = row['session'].session_summary['curves'][k].rawdata
                        tmp_cnts['{0}_{1}'.format(l,k)] = 1
                    else:
                        tmp_dict[k] = pd.concat([tmp_dict[k],row['session'].session_summary['curves'][k].rawdata],ignore_index=True)
                        tmp_cnts['{0}_{1}'.format(l,k)] += 1

                if pool_cnt >= pool_depth:
                    break
        
            for kk in tmp_dict.keys():
                name = '{0}_{1}'.format(l,kk)
                tmp_curve = WheelCurve(kk,tmp_dict[kk])
                self.pooled_curves[l][kk] = (tmp_curve,tmp_cnts[name])

    def append_plot_data(self,df_in):
        #set edge colors
        df = df_in
        df['edge'] = df['level'].apply(lambda x: 'blue' if 'opto' in x else 'white')
        df.loc[df_in['level']=='level1','edge'] = 'silver'
        df.loc[df_in['level']=='level2','edge'] = 'orangered'
        df.loc[df_in['level']=='level3','edge'] = 'magenta'

        return df

    def init_plotter(self,plot_params=None,savefig=False):
        if plot_params is None:
            plot_params = dict(fig_size=(15, 15),
                               font_size=20,
                               title_size=23,
                               line_width=9,
                               err_width=3,
                               bar_width=1,
                               marker_size=10,
                               edge_width=2,
                               legend_fontsize=24)
        self.plot_params = plot_params
        self.savefig = savefig
        self.plot_data = self.append_plot_data(self.behavior_data)
        
    def plot(self,plotkey='performance',size_override=None,savefig=None,ax=None):
        if type(plotkey) is not list:
            plotkey = [plotkey]
        if size_override is None:
            fig = plt.figure(figsize=self.plot_params['fig_size'])
        else:
            fig = plt.figure(figsize=size_override)
            
        if savefig is None:
            savefig = self.savefig
        
        if plotkey[0] is 'performance':
            if ax is None:
                ax = fig.add_subplot(1, 1, 1)
            bd = self.plot_data
            session_dates = bd['session_date']
            duplicate_dates = find_duplicates(session_dates)

            if duplicate_dates is not None:
                str_session_dates = []
                sesh_i = 1
                for x in session_dates:
                    if x in duplicate_dates:
                        str_session_dates.append(x.strftime('%d.%m.%y-{0}'.format(sesh_i)))
                        sesh_i += 1
                    else:
                        str_session_dates.append(x.strftime('%d.%m.%y'))
                        sesh_i = 1
            else:   
                str_session_dates = [date.strftime('%d.%m.%y') for date in session_dates]


            left = np.asarray([sesh.session_summary['left_profile'] for sesh in bd['session']])
            right = np.asarray([sesh.session_summary['right_profile'] for sesh in bd['session']])

            #set edge colors
            bd['edge'] = bd['level'].apply(lambda x: 'blue' if 'opto' in x else 'white')
            bd.loc[bd['level']=='level1','edge'] = 'silver'
            bd.loc[bd['level']=='level2','edge'] = 'orangered'
            bd.loc[bd['level']=='level3','edge'] = 'magenta'


            #LEFT
            #correct
            ax.bar(str_session_dates, left[:,0], 
                    width=self.plot_params['bar_width'],
                    color='darkgreen',
                    edgecolor=bd['edge'],
                    linewidth=self.plot_params['edge_width'],
                    label='Correct')
            #incorrect
            ax.bar(str_session_dates, left[:,1], 
                    width=self.plot_params['bar_width'],
                    bottom=left[:,0],
                    color='maroon',
                    edgecolor=bd['edge'],
                    linewidth=self.plot_params['edge_width'],
                    label='Incorrect')
            #non answer
            ans_hl = np.add(left[:,0], left[:,1]).tolist()
            ax.bar(str_session_dates, left[:,2], 
                    width=self.plot_params['bar_width'],
                    bottom=ans_hl,
                    color='k',
                    edgecolor=bd['edge'],
                    linewidth=self.plot_params['edge_width'],
                    label='No Go') 

            #RIGHT
            #correct
            ax.bar(str_session_dates, right[:,0], 
                    width=self.plot_params['bar_width'],
                    color='darkgreen',
                    edgecolor=bd['edge'],
                    linewidth=self.plot_params['edge_width'])
            #incorrect
            ax.bar(str_session_dates, right[:,1], 
                    width=self.plot_params['bar_width'],
                    bottom=right[:,0],
                    color='maroon',
                    edgecolor=bd['edge'],
                    linewidth=self.plot_params['edge_width'])
            #non answer
            ans_hr = np.add(right[:,0], right[:,1]).tolist()
            ax.bar(str_session_dates, right[:,2], 
                    width=self.plot_params['bar_width'],
                    bottom=ans_hr,
                    color='k',
                    edgecolor=bd['edge'],
                    linewidth=self.plot_params['edge_width']) 

            for i,r in bd.iterrows():
                if len(r['rig'])==2:
                    y_offset = ans_hr[i]+right[i,2]+8
                    ax.text(str_session_dates[i], y_offset, r['rig'], fontsize=15)

            #midline
            ax.set_ylim([ax.get_ylim()[0]-15, ax.get_ylim()[1]+15])
            ax.plot(ax.get_xlim(),[0,0],'white',linewidth=2)
            ax.set_xlabel('Session Dates', fontsize=self.plot_params['font_size'])
            ax.tick_params(axis='x', rotation=90)
            ax.set_ylabel('Response Distribution(L<=>R)', fontsize=self.plot_params['font_size'])
            ax.set_title('{0} Performance'.format(self.animalid), fontsize=self.plot_params['font_size'])
            ax.tick_params(axis='both', size=self.plot_params['font_size'])
            ax.tick_params(labelsize=self.plot_params['font_size'])
            ax.grid(b=True,axis='y')
            ax.grid(b=True,axis='x',linewidth=0.5)
            ax.set_axisbelow(b=True)
            ax.legend(loc='upper left',fontsize=self.plot_params['font_size'])
            
            save_name = 'trainingSummary.pdf'
            
        elif plotkey[0] is 'psychometric':
            # performance with error bars for different levels
            levels = self.pooled_curves.keys() 
            #only output single level
            if len(plotkey) == 2:
                levels = [x for x in levels if plotkey[1] in x]

            if len(levels) == 0:
                display('Nothing to plot for {0} in {1}'.format(self.animalid,plotkey[1]))
                return None

            for i,l in enumerate(levels,start=1):
                if len(levels) == 1:
                    ax = fig.add_subplot(1,1,1)
                    save_name = '{0}_psychometric.pdf'.format(l)
                else:
                    ax = fig.add_subplot(np.ceil(len(levels)/2), 2, i)
                    save_name = 'combined_psychometric.pdf'

                for k in self.pooled_curves[l].keys(): 

                    tmp_curve = self.pooled_curves[l][k][0]

                    ax.errorbar(100 * tmp_curve.contrast_x, tmp_curve.proportion_right, tmp_curve.binomial_right,
                                marker='o',
                                c=area_colors[tmp_curve.name],
                                linewidth=0,
                                markersize=self.plot_params['marker_size'],
                                markeredgecolor='k',
                                elinewidth=self.plot_params['err_width'],
                                ecolor='k',
                                capsize=5,
                                label='{0}({1} sessions)'.format(k,self.pooled_curves[l][k][1]))

                    ax.plot(100 * tmp_curve.contrast_x_fitted, tmp_curve.proportion_right_fitted,
                                c=area_colors[tmp_curve.name],
                                linewidth=self.plot_params['line_width'])

                #midlines
                ax.plot([0, 0], [-0.1, 1.1], 'k', linestyle=':', linewidth=2)
                ax.plot([-105, 105], [0.5, 0.5], 'k', linestyle=':', linewidth=2)

                ax.set_xlim([-105, 105])
                ax.set_ylim([-0.1, 1.1])
                ax.set_xlabel('Contrast Values', fontsize=self.plot_params['font_size'])
                ax.set_ylabel('Prob. Choosing R(%)', fontsize=self.plot_params['font_size'])
                ax.set_title('{0} {1} Curve'.format(l,k), fontsize=self.plot_params['font_size'])
                ax.tick_params(axis='both', size=self.plot_params['font_size'])
                ax.tick_params(labelsize=self.plot_params['font_size'])
                ax.legend(loc='upper left', borderpad=0.1, labelspacing=0.4, prop={'size': self.plot_params['legend_fontsize']})
                ax.spines['right'].set_visible(False)
                ax.spines['top'].set_visible(False)

            fig.suptitle('{0} Psychometric Curves [{1},{2}]'.format(self.animalid,self.criteria['answered_trials'],self.criteria['answered_correct']), fontsize=self.plot_params['title_size'])
            fig.tight_layout(rect=[0, 0.03, 1, 0.95])

        elif plotkey[0] is 'health':
            if ax is None:
                ax = fig.add_subplot(1, 1, 1)

            bd = self.plot_data
            session_dates = bd['session_date']
            duplicate_dates = find_duplicates(session_dates)

            if duplicate_dates is not None:
                str_session_dates = []
                sesh_i = 1
                for x in session_dates:
                    if x in duplicate_dates:
                        str_session_dates.append(x.strftime('%d.%m.%y-{0}'.format(sesh_i)))
                        sesh_i += 1
                    else:
                        str_session_dates.append(x.strftime('%d.%m.%y'))
                        sesh_i = 1
            else:   
                str_session_dates = [date.strftime('%d.%m.%y') for date in session_dates]

            # #threshold weight line
            # ax.plot(str_session_dates, bd['thresh_weight'])

            # weight progression
            ax.plot(str_session_dates,bd['weight'], color='firebrick',
                marker='o',markersize=12,linewidth=self.plot_params['line_width'], label='Weight')

            ax2 = ax.twinx()

            # water consumption
            water_on_rig = [sesh.session_summary['water_on_rig'] for sesh in bd['session']]
            ax2.bar(str_session_dates,water_on_rig,width=0.75,
                color='deepskyblue',label='Water on rig',zorder=1,alpha=0.5)

            ax2.bar(str_session_dates,bd['extra_water'],width=0.75,bottom=water_on_rig,
                color='steelblue',label='Extra Water',alpha=0.5)


            # for r in bd.itertuples():
            #     ax2.text(str_session_dates[r.Index], r.water_consumption+10, '|',
            #         horizontalalignment='center',color=r.edge, fontsize=20,fontweight='extra bold')

            ax.set_ylim([10,40])
            ax.set_xlabel('Session Dates', fontsize=self.plot_params['font_size'])
            ax.tick_params(axis='x', rotation=90)
            ax.set_ylabel('Mouse Weight(g)', fontsize=self.plot_params['font_size'])
            ax.set_title('{0} Health'.format(self.animalid), fontsize=self.plot_params['font_size']+3)
            ax.tick_params(axis='both', size=self.plot_params['font_size'])
            ax.tick_params(labelsize=self.plot_params['font_size'])
            
            ax2.set_ylabel('Consumed Water(uL)',fontsize=self.plot_params['font_size'])
            ax2.tick_params(axis='y', size=self.plot_params['font_size'])
            ax2.grid(alpha=0.5,axis='y')

            fig.legend(loc='upper left',bbox_to_anchor=(0,1), bbox_transform=ax.transAxes,fontsize=self.plot_params['font_size'])

            save_name = 'mouseHealth.pdf'

        
        if savefig:
            savepath = pjoin(self.analysisfolder,self.session_list[-1][0],save_name).replace("\\","/")
            display('{0} graph saved in {1}'.format(plotkey[0],savepath))
            
            fig.savefig(savepath, bbox_inches='tight')
        return ax

def main():
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Wheel Behavior Data Parsing Tool')

    parser.add_argument('id',metavar='animalid',
                        type=str,help='Animal ID (e.g. KC020)')
    parser.add_argument('-d','--date',metavar='dateinterval',
                        type=str,help='Analysis start date (e.g. 191124)')
    parser.add_argument('-c','--criteria',metavar='criteria',default=[20,0],
                        type=str, help='Criteria dict for analysis thresholding, delimited list input')
    
    '''
    wheelbehave -d 200501 -c "20, 10" KC028
    '''

    opts = parser.parse_args()
    animalid = opts.id
    dateinterval = opts.date
    tmp = [int(x) for x in opts.criteria.split(',')]
    criteria = dict(answered_trials=tmp[0],
                    answered_correct=tmp[1])

    display('Updating Wheel Behavior for {0}'.format(animalid))
    display('Set criteria: {0}: {1}\n\t\t{2}: {3}'.format(list(criteria.keys())[0],
                                                  list(criteria.values())[0],
                                                  list(criteria.keys())[1],
                                                  list(criteria.values())[1]))
    w = WheelBehavior(animalid=animalid, dateinterval=dateinterval, criteria=criteria)

if __name__ == '__main__':
    main()
