
## wheelypy

Code to parse log files from wheel experiments, analyse sessions and overall training of mice.
Can be run as bash script after going into the wheelpy directory and running  ``` python setup.py develop ```

### Dependencies
numpy
scipy
matplotlib
natsort

#### Wheel Parser
Parses the .log files in the presentation folder to a .csv(or .mat) file for further processing
Can be used as a standalone script

Example usage:
```
wheelparse 200303_KC020_wheel_KC
```

#### Wheel Session
Session analysis and plotting data

Example usage:
```
wheelsesh 200303_KC020_wheel_KC
```


#### Wheel Behavior
Analysis of multiple sessions. Starting date or a date interval can be given and threshold values to omit performances can be set

Example usage:
```
wheelbehave -d 200501 -c "20, 10" KC028
```

