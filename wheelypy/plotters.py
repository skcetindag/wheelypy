import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patheffects as path_effects
from wheelypy.utils import find_nearest

contrast_colors = {0:'tab:red',
                   0.125:'tab:orange',
                   0.25:'tab:olive',
                   0.5:'tab:green',
                   1:'tab:blue'}

# color codes for different stimuli (areas)
area_colors = {'AL_normal':'tab:orange',
               'AL_opto':'tab:blue',
               'PM_normal':'tab:green',
               'PM_opto':'tab:cyan',
               'grating_normal':'tab:orange',
               'grating_opto':'tab:blue'}

def plotPsychometric(curves,title='',ax=None, barebones=False,*args,**kwargs):
    """
    """
    fontsize = kwargs.get('fontsize',20)
    if ax is None:
        ax = fig.add_subplot(1,1,1)

    for key,curve in curves.items():
        ax.errorbar(100 * curve.contrast_x, curve.proportion_right, curve.binomial_right,
                    marker='o',
                    c=area_colors[key],
                    linewidth=0,
                    markersize=kwargs.get('markersize',15),
                    markeredgecolor='k',
                    elinewidth=kwargs.get('elinewidth',3),
                    ecolor='k',
                    capsize=5,
                    label='{0}'.format(key))

        ax.plot(100 * curve.contrast_x_fitted, curve.proportion_right_fitted,
                c=area_colors[key],
                linewidth=kwargs.get('linewidth',9))
        #midlines 
        ax.plot([0, 0], [-0.1, 1.1], 'k', linestyle=':', linewidth=2)
        ax.plot([-105, 105], [0.5, 0.5], 'k', linestyle=':', linewidth=2)

        ax.set_xlim([-105, 105])
        ax.set_ylim([-0.1, 1.1])
        ax.set_xlabel('Contrast Value', fontsize=fontsize)
        ax.tick_params(labelsize=fontsize)
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        if not barebones:
            ax.set_ylabel('Prob. Choosing R(%)', fontsize=fontsize)
            ax.set_title('{0} Psychometric Curve'.format(title), fontsize=fontsize+3)
            ax.legend(loc='upper left', borderpad=0.1, labelspacing=0.4, prop={'size':fontsize})
        return ax

def plotTrajectories(side_traj,s_limit=None,title='',ax=None,*args,**kwargs):

    fontsize = kwargs.get('fontsize',20)
    plt_mode = kwargs.get('plt_mode','sem')

    if ax is None:
        ax = fig.add_subplot(nrow, ncol, i)
    # loops all contrasts
    legends=[]
    for c in side_traj.trajectories.keys():
        contrast_trj = side_traj.trajectories[c]
        if contrast_trj['nonopto'] is not None:
            avg_x = side_traj.averages[c]['nonopto'][:, 0]
            avg_y = side_traj.averages[c]['nonopto'][:, 1]
            sem_y = side_traj.sems[c]['nonopto'][:,0]

            if s_limit is not None:
                avg_x = avg_x[0:find_nearest(avg_x,s_limit)[0]]
                avg_y = avg_y[0:len(avg_x)]
                sem_y = sem_y[0:len(avg_x)]

            # nonopto avg
            nonopto_line, = ax.plot(avg_x, avg_y,
                    linewidth=kwargs.get('linewidth',5),
                    color=contrast_colors[c],
                    label='{0}%'.format(100*c),
                    alpha=1,
                    zorder=2)

            legends.append(nonopto_line)

            if plt_mode == 'individual':
            # individual trajectories
                for t in range(contrast_trj['nonopto'].shape[2]):
                    # nonopto
                    xx = contrast_trj['nonopto'][:, 0, t]
                    yy = contrast_trj['nonopto'][:, 1, t]
                    if s_limit is not None:
                        xx = xx[0:find_nearest(xx,s_limit)[0]]
                        yy = yy[0:len(xx)]
                    ax.plot(xx, yy,
                            linewidth=3,
                            color=contrast_colors[c],
                            alpha=0.3)
            elif plt_mode == 'sem':
                # standart error mean area
                ax.fill_between(avg_x,avg_y-sem_y,avg_y+sem_y,
                                color=contrast_colors[c],
                                alpha=0.3)

        if side_traj.optogenetic:
            if contrast_trj['opto'] is not None:
                avg_x = side_traj.averages[c]['opto'][:, 0]
                avg_y = side_traj.averages[c]['opto'][:, 1]
                sem_y = side_traj.sems[c]['opto'][:,0]

                if s_limit is not None:
                    avg_x = avg_x[0:find_nearest(avg_x,s_limit)[0]]
                    avg_y = avg_y[0:len(avg_x)]
                    sem_y = sem_y[0:len(avg_x)]

                # opto avg
                opto_line, = ax.plot(avg_x, avg_y,
                        linewidth=kwargs.get('linewidth',5),
                        color=contrast_colors[c],
                        path_effects=[
                            path_effects.Stroke(linewidth=kwargs.get('linewidth',6) + 2, foreground="b"),
                            path_effects.Normal()],
                        label='{0}% Opto'.format(100*c),
                        alpha=1,
                        zorder=2)

                if plt_mode=='individual':
                # individual trajectories
                    for t in range(contrast_trj['opto'].shape[2]):
                        # opto
                        xx = contrast_trj['opto'][:, 0, t]
                        yy = contrast_trj['opto'][:, 1, t]
                        if s_limit is not None:
                            xx = xx[0:find_nearest(xx,s_limit)[0]]
                            yy = yy[0:len(xx)]
                        ax.plot(xx, yy,
                                linewidth=2,
                                color=contrast_colors[c],
                                path_effects=[path_effects.Stroke(linewidth=2.5, foreground="b", alpha=0.3),
                                              path_effects.Normal()],
                                alpha=0.3)
                elif plt_mode=='sem':
                    # standart error mean area
                    ax.fill_between(avg_x,avg_y-sem_y,avg_y+sem_y,
                            color=contrast_colors[c],
                            alpha=0.3)

                legends.append(opto_line)

    # closed loop start line
    ax.plot(ax.get_xlim(),[0,0],'k',linewidth=2)
    ax.plot([0,0], ax.get_ylim(), 'k', linestyle=':', linewidth=2)
    # ax.set_ylim([-700, 700])
    ax.set_xlabel('Time (ms)', fontsize=fontsize)
    ax.set_ylabel('Wheel Position', fontsize=fontsize)
    ax.set_title('{0} Wheel Trajectory'.format(side_traj.name), fontsize=fontsize+3)
    ax.tick_params(labelsize=fontsize)
    ax.set_yticks([])
    ax.legend(handles=legends)
    return ax

def plotPerformance(df,title='',ax=None, barebones=False,*args,**kwargs):
    """
    """
    fontsize = kwargs.get('fontsize',20)

    if ax is None:
        ax = fig.add_subplot(1, 1, 1)

    ax.plot(df.openloopstart/60000,df.running_accuracy*100,
            label='Normal Response',
            color='darkgreen',
            linewidth=kwargs.get('linewidth',9))

    # ax.plot(df.trial_no,running_percent_wheel,
    #         label='Wheel Response',
    #         linewidth=kwargs.get('linewidth',9))

    ax.set_ylim([0, 100])
    
    ax.set_xlabel('Time(min)', fontsize=fontsize)
   
    ax.tick_params(labelsize=fontsize)
    ax.grid(alpha=0.5,axis='both')

    if not barebones:
        ax.set_ylabel('Accuracy(%)', fontsize=fontsize)
        ax.set_title('{0} Session Performance'.format(title),fontsize=fontsize)
        ax.legend(loc='upper right',fontsize=fontsize)
    return ax

def plotResponseTime(novels,title='',ax=None,barebones=False,*args,**kwargs):
    """"""
    fontsize = kwargs.get('fontsize',20)

    if ax is None:
        ax = fig.add_subplot(1, 1, 1)

    left = novels[novels['stim_side']==-1]
    right = novels[novels['stim_side']==1]

    indivs = kwargs.get('plot_indivs',False)


    #stim on left
    avg_left = np.mean(left['response_latency'])
    if indivs:
        ax.plot(left['openloopstart']/60000,left['response_latency']/1000,
                linewidth=kwargs.get('linewidth',3),
                color='firebrick',
                alpha=0.33)

    ax.plot(left['openloopstart']/60000,left['running_latency']/1000,
            color='firebrick',
            linewidth=kwargs.get('linewidth',5),
            label='Left Running Avg.(N=5)')


    #stim on right
    avg_right = np.mean(right['response_latency'])
    if indivs:
        ax.plot(right['openloopstart']/60000,right['response_latency']/1000,
                linewidth=kwargs.get('linewidth',3),
                color='teal',
                alpha=0.33)

    ax.plot(right['openloopstart']/60000,right['running_latency']/1000,
            color='teal',
            linewidth=kwargs.get('linewidth',5),
            label='Right Running Avg.(N=5)')

    ax.set_yscale('log')

    #base stim time line (TODO GET BASE STIM TIME)
    # if self.session_summary['level'] == 'level0':
    #     ax.plot([0,self.session_summary['novel_trials']],[2500,2500],
    #         color='black',linewidth=3,alpha=0.5,label='Base Stim Time(Avg.={0:.2f}ms)'.format(2500))

    # ax.plot(novels['trial_no'],run_avg,color='black',linewith=3,label='Running avg(N={0})'.format(N))

    ax.set_xlabel('Time(min)', fontsize=fontsize)
    
    
    ax.tick_params(labelsize=fontsize)
    ax.grid(alpha=0.5,axis='both')
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)

    if not barebones:
        ax.set_ylabel('Response Time (s)', fontsize=fontsize)
        ax.legend(loc='upper left',fontsize=fontsize)
        # ax.set_title('{0} Stim Side Responses'.format(self.session_summary['level']), fontsize=self.plot_params['title_size'])
    return ax

def plotTrialProgress(df,title='',ax=None,*args,**kwargs):
    """"""
    fontsize = kwargs.get('fontsize',20)

    if ax is None:
        ax = fig.add_subplot(1, 1, 1)

    df['y_offset'] = df['stim_side']*df['correction'].apply(lambda x: 1+x/5)
    df['color'] = 'k'
    df.loc[df['answer']==1,'color']='g'
    df.loc[df['answer']==-1,'color']='r'

    df['opto_edge'] = df['color'] 
    df.loc[df['opto']==1,'opto_edge']='b'

    df['trial_no_seperated'] = df['trial_no']
    df['trial_no_str'] = df['trial_no'].apply(lambda x: str(x))

    #legit trials
    legit_data = df[df['correction']==0]
    ax.scatter(legit_data['trial_no_seperated'],legit_data['y_offset'],
                c=legit_data['color'], 
                marker='o',
                edgecolor=legit_data['opto_edge'],
                label='Novel Trials')
    #corrections
    corr_data = df[df['correction']!=0]
    ax.scatter(corr_data['trial_no_seperated'],corr_data['y_offset'],
                c=corr_data['color'], 
                marker='+',
                label='Repeat Trials')
    #midline
    ax.plot([-1, df['trial_no_seperated'].iloc[-1]+1], [0, 0], 'k', linestyle=':', linewidth=2)
    
    ax.set_ylim([-3, 3])
    ax.set_xlabel('Trial No', fontsize=fontsize)
    ax.set_ylabel('Stim On\n Left    Right', fontsize=fontsize)
    # ax.legend(loc='upper left',fontsize=self.plot_params['font_size'])
    # ax.set_title('Trial progression\nPC={0:.2f}%({1:.2f}%)'.format(self.session_summary['answered_correct'],self.session_summary['all_correct']), 
    #              fontsize=fontsize+3)
    ax.tick_params(labelsize=fontsize)
    # ax.yaxis.set_major_locator(plt.NullLocator())
    ax.set_yticks([])
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.legend(loc='upper right',fontsize=fontsize)
    return ax

def plotStackedSummary(df, title='',days=[2,5,10,15] ,ax=None,*args,**kwargs):
    """ """
    fig = plt.figure(figsize=(15,15))
    fontsize = kwargs.get('fontsize',15)
    nrows = 3
    ncols = len(days)
    start_session = df[df['level']!='level0']['session_no'].iloc[0]
    for row in range(nrows):
        for col,day in enumerate(days,1):
            ax_idx = row * ncols + col
            ax = fig.add_subplot(nrows, ncols, ax_idx)

            if day == -1:
                # most recent training
                day_row = df.tail(1)
                day = int(day_row['session_no'].iloc[0] - start_session)
            else:
                day_row = df[df['session_no']==start_session+day-1]

            day_session = day_row['session'].iloc[0]
            day_date = day_row['session_date'].iloc[0].strftime('%d.%m.%y')
            novels = day_session.session_data_novel
            # first row, trial duration
            if row == 0:
                ax.set_title('Day {0}: {1} Trials\n{2}'.format(day,day_session.session_summary['novel_trials'],day_date),fontsize=fontsize)
                plotResponseTime(novels,title=title,ax=ax, barebones=True,*args,**kwargs)
            if row == 1:
                plotPerformance(novels, title='',ax=ax,barebones=True,*args,**kwargs)
            if row == 2:
                curves = day_session.session_summary['curves']
                plotPsychometric(curves, title='', ax=ax, barebones=True,*args,**kwargs) 


    return fig
            # ax.set_title('Day {0}: {1}'.format(day,day_row['all_trials']))
            # ax.set_xlabel('Time(min)', fontsize=fontsize)
            # ax.set_ylabel('Trial duration', fontsize=fontsize)
            # ax.spines['right'].set_visible(False)
            # ax.spines['top'].set_visible(False)
            # ax.legend(loc='upper right',fontsize=fontsize)




# class SessionPlotter(WheelExperiment):
#     def __init__(self):
#         pass