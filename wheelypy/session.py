import time
import json
import pandas as pd
from os.path import join as pjoin
from wheelypy.utils import *


class Session(object):
    """ A base Session object to be used in analyzing training/experiment sessions
        :param sessiondir: directory of the session inside the presentation folder(e.g. 200619_KC33_wheel_KC)
        :param load_flag:  flag to either load previously parsed data or to parse it again
        :param spit_mat:   flag to make the parser also output a .mat file to be used in MATLAB  scripts
        :type sessiondir:  str
        :type load_flag:   bool
        :type spit_mat:    bool
    """
    def __init__(self, sessiondir, load_flag=False, spit_mat=False):
        self.sessiondir = sessiondir
        self.session_data = None
        self.load_flag = load_flag
        self.spit_mat = spit_mat

        start = time.time()
        self.init_data_paths()
        self.get_session_version()

        self.opts, self.params = parseProtocolFile(self.data_paths['prot'])
        self.prefs = parsePref(self.data_paths['prefs'])

        self.read_data()
        end = time.time()
        display('Read data in {0:.2f} seconds'.format(end-start))

    def init_data_paths(self):
        """ Initializes the relevant data paths (log,pref,prot and saves)"""
        self.sessionpath = pjoin('J:/data/presentation',self.sessiondir).replace("\\","/")
        self.savepath = pjoin('J:/data/analysis',self.sessiondir).replace("\\","/")
        if os.path.exists(self.sessionpath):
            if not os.path.exists(self.savepath):
                # make dirs
                os.makedirs(self.savepath)
                display('Save path does not exist, created save folder at {0}'.format(self.savepath))

            self.data_paths = {'data': pjoin(self.savepath,'sessionData.csv').replace("\\","/"),
                               'meta': pjoin(self.savepath,'sessionMeta.json').replace("\\","/"),
                               'stat': pjoin(self.savepath,'sessionStats.json').replace("\\","/")}
            try:
                for s_file in os.listdir(self.sessionpath):
                    extension = os.path.splitext(s_file)[1]
                    if extension=='.prot':
                        self.data_paths['prot'] = pjoin(self.sessionpath,s_file).replace("\\","/")
                    elif extension == '.prefs':
                        self.data_paths['prefs'] = pjoin(self.sessionpath,s_file).replace("\\","/")
                    elif extension == '.riglog':
                        self.data_paths['riglog'] = pjoin(self.sessionpath,s_file).replace("\\","/")
                    elif extension=='.stimlog':
                        self.data_paths['stimlog'] = pjoin(self.sessionpath,s_file).replace("\\","/")
                    elif extension=='.log':
                        self.data_paths['log'] = pjoin(self.sessionpath,s_file).replace("\\","/")

            except FileNotFoundError:
                if s_file.endswith('.riglog') or s_file.endswith('.stimlog') or s_file.endswith('.log'):
                    display('{0} does not exist, trying .log file from old pyvstim code...'.format(s_file))
                    if s_file.endswith('.log'):
                        if '.log' not in self.data_paths.keys():
                            self.data_paths['stimlog'] = pjoin(self.sessionpath,s_file).replace("\\","/")
                        else:
                            display('.log already found and added to relevant paths.')
                    else:
                        display('{0} is missing. THIS IS BAD, SOMETHING WENT WRONG DURING EXPERIMENT!!'.format(s_file))
                        raise FileNotFoundError
                else:
                    # .prot or .pref missing
                    display('{0} is missing, that should not happen...')
                    # TODO: add a copy from user directory feature in here in the future
        else:
            raise FileNotFoundError('Session directory {0} does not exist!'.format(self.sessiondir))

    def read_data(self):
        if self.logversion == 'pyvstim':
            self.data, self.comments = parseVStimLog(self.data_paths['log'])
        elif self.logversion == 'stimpy':
            # because there are two different log files now, they have to be combined into a single data variable
            stim_data, stim_comments = parseStimpyLog(self.data_paths['stimlog'])
            rig_data, rig_comments = parseStimpyLog(self.data_paths['riglog'])
            self.data = {**stim_data, **rig_data}
            self.comments = stim_comments + rig_comments
            self.data = extrapolate_time(self.data)

    def get_session_version(self):
        """ Gets the session logging version from existing log file extension."""
        # TODO: Look into this. Version should be printed in stimpy data maybe???
        # self.logversion = self.comments[0].replace('Version','').replace('#','').strip(' ')
        # display('Parsing log version {0}'.format(self.logversion))
        if 'log' in self.data_paths.keys():
            self.logversion = 'pyvstim'
        elif 'stimlog' in self.data_paths.keys() and 'riglog' in self.data_paths.keys():
            self.logversion = 'stimpy'

    def isSaved(self):
        """ Initializes the necessary save paths and checks if data already exists, if it doesn't creates the savepath

            :return: Returns a boolean that 
            :rtype:  bool
        """
        self.isLoadable = False
        # the 'stim_pos and 'wheel' columns are saved as lists in DataFrame columns!
        if os.path.exists(self.savepath):
            if os.path.exists(self.data_paths['data']):
                self.isLoadable = True
                display('Found saved data: {0}'.format(self.data_paths['data']))
            else:
                display('{0} exists but no data file is present...'.format(self.savepath))
        else:
            display('THIS SHOULD NOT HAPPEN')

        return self.isLoadable

    def save_session(self):
        """ Saves the session data as .csv (and .mat file if desired) 
            metadata  and session_summary as .json
        """
        # np.set_printoptions(threshold='nan')
        self.session_data.to_csv(self.data_paths['data'],index=False)

        with open(self.data_paths['meta'],'w') as fp:
            jsonstr = json.dumps(self.session_meta,indent=4, sort_keys=True)
            fp.write(jsonstr)
        display('Saving metadata...')

        toJson_summary = {}
        for k,v in self.session_summary.items():
            if k == 'curves' or k == 'trajectories':
                continue
            toJson_summary[k] = v
        with open(self.data_paths['stat'],'w') as fp:
            jsonstr = json.dumps(toJson_summary,indent=4, sort_keys=True)
            fp.write(jsonstr)
        display('Saving session stats...')

        if self.spit_mat:
            self.save_as_mat()
            display('Saving .mat file')
        
    def load_session_data(self):
        """Loads the data from J:/analysis/<exp_folder> as a pandas data frame

            :return: Loaded data
            :rtype:  DataFrame
        """
        data = pd.read_csv(self.data_paths['data'])
        data['closedloopdur'] = data['closedloopdur'].apply(eval)
        data['stim_pos'] = data['stim_pos'].apply(eval)
        data['wheel'] = data['wheel'].apply(eval)
        return data 

    def save_as_mat(self):
        """Helper method to convert the data into a .mat file"""
        import scipy.io as sio
        datafile = pjoin(self.savepath,'sessionData.mat').replace("\\","/")
        save_dict = {name: col.values for name, col in self.session_data.items()}
        sio.savemat(datafile, save_dict)
