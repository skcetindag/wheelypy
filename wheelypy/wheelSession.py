import os
import copy
import json
import time
from os.path import join as pjoin
from wheelypy.utils import *
from wheelypy.wheelAnalysis import WheelCurve,WheelTrajectory
from wheelypy.plotters import *
from wheelypy.experiment import Experiment

class WheelSession(Sesion):
    """ A class to parse the experiment data (.log file) for training & experiments

        Saves the parsed data as a .csv file inside the save directory
        Saves the metadata and session stats as .json inside the save directory

        :param sessiondir: directory of the session inside the presentation folder(e.g. 200619_KC33_wheel_KC)
        :type sessiondir:  str
    """
    def __init__(self,sessiondir,wheel_response_flag=True, autoplot=False, *args,**kwargs):
        super().__init__(sessiondir,*args,**kwargs)
        
        start = time.time()
        self.wheel_response_flag = wheel_response_flag
        self.autoplot = autoplot
        self.read_session_level()
        self.set_version_keys()
        self.init_metadata()

        if self.isSaved() and self.load_flag:
            #load data converts the columns automatically
            display('Loading from {0}'.format(self.savepath))
            self.session_data= self.load_session_data()
        else:
            display('Analyzing and saving data to {0}'.format(self.savepath))
            self.session_data = self.get_session_data()

        self.session_meta['water_given'] = int(np.sum(self.session_data['reward']))

        end = time.time()
        display('Done! t={0:.2f} s'.format(end-start))

        self.save_session()
        # the 'stim_pos and 'wheel' columns need to be saved as lists in DataFrame columns!
        # They are first saved/loaded as lists and then converted into numpy arrays for futher analysis
        self.session_data = self.convert_columns(self.session_data)

        if self.autoplot:
            self.plot_all()

    def convert_columns(self,df_in):
        """ Converts the columns of the input dataframe to np.arrays. 
        This is to solve the issue with save/load as list instead of np.array

         :param df_in: DataFrame that will have it's columns change
         :type df_in:  DataFrame
        """
        df_in['stim_pos'] = df_in['stim_pos'].apply(lambda x: np.asarray(x))
        df_in['wheel'] = df_in['wheel'].apply(lambda x: np.asarray(x))
        return df_in

    def read_session_level(self):
        """Gets the level from .prot file name"""
        tmp = os.path.splitext(self.data_paths['prot'])[0]
        self.level = tmp.split('_')[-1]

    def set_version_keys(self):
        """ Sets the important keys according to code version"""
        self.transition_keys = {'openloopstart':'1->2',
                                'closedloopstart':'2->3',
                                'correct':'3->4',
                                'incorrect':'3->5',
                                'trialend':'6->0'}

        if self.logversion == 'pyvstim':
            self.column_keys = {'code':'code',
                                'elapsed':'presentTime',
                                'trialNo':'iStim',
                                'newState':'iTrial',
                                'oldState':'iFrame',
                                'trialType':'contrast',
                                'stateElapsed':'blank'}
            self.transition_keys['nonanswer'] = '3->7'
            self.transition_keys['earlyanswer'] = '2->5'
            self.transition_keys['correction'] = '7->1'
            if self.level == 'level0':
                self.transition_keys['closedloopstart'] = '2->8'
                self.transition_keys['correct'] = '8->4'
        
        elif self.logversion == 'stimpy':
            self.column_keys = {}
            for c in self.data['stateMachine'].columns:
                if c == 'cycle':
                    self.column_keys['trialNo'] = c
                else:
                    self.column_keys[c] = c
            self.transition_keys['earlyanswer'] = 'na'
            self.transition_keys['correction'] = 'na'
            self.transition_keys['nonanswer'] = 'na'

        display('Setting global indxing keys for {0} logging'.format(self.logversion))

    def init_metadata(self):
        """ """
        self.date = dt.strftime(dt.strptime(self.sessiondir.split('/')[-1].split('_')[0], '%y%m%d').date(), '%d %b %y')
        runname = os.path.splitext(self.data_paths['prefs'])[0]
        
        if 'opto' in runname:
            opto = True
        else:
            opto = False

        self.experimentname = self.sessiondir.split('/')[-1]
        self.animalid = self.sessiondir.split('_')[1]

        self.session_meta = {'date'           : self.date,
                             'experimentname' : self.experimentname,
                             'animalname'     : self.animalid,
                             'starttime'      : self.comments[2].split('-')[-1].strip(' '),
                             'level'          : self.level,
                             'contrastset'    : self.opts['contrastVector'],
                             'wheelgain'      : self.opts['wheelGain'],
                             'rig'            : self.prefs['tmpFolder'].split('\\')[-1],
                             'runname'        : runname,
                             'opto'           : opto}
          
    def add_data_interval(self,start,end):
        """ Adds data from other types of logged data(opto, vstim, lick etc) in a given time interval
            :param start: Starting time of data interval 
            :param end:   End time of data interval
            :type start: int,float
            :type end:   int, float
        """
        interval_dict = {}
        vstim = self.data['vstim']        
        if self.logversion == 'pyvstim':
            vstim_interval = vstim[(vstim['presentTime'] >= start) & (vstim['presentTime'] <= end)].dropna()
            # get contrast
            interval_dict['contrast'] = vstim_interval['iFrame'].iloc[0]
            
            # get stim side
            interval_dict['stim_side'] = np.sign(vstim_interval['iTrial'].iloc[0])

            # get stim movement
            interval_dict['stim_pos'] = np.asarray(vstim_interval['iTrial']).tolist()

            # get sftf
            interval_dict['spatial_freq'] = vstim_interval['blank'].iloc[0]
            if 'contrast' in vstim_interval.keys():
                interval_dict['temporal_freq'] = vstim_interval['contrast'].iloc[0]
            else:
                interval_dict['temporal_freq'] = 0

        elif self.logversion == 'stimpy':
            vstim_interval = vstim[(vstim['presentTime'] >= start/1000) & (vstim['presentTime'] <= end/1000)].dropna()

            # fix for swapped left right logging(for data before 26/01/2021)
            vstim_columns_tofix = list(vstim_interval.columns)
            idx_posl = vstim_columns_tofix.index('posx_l')
            idx_posr = vstim_columns_tofix.index('posx_r')
            if idx_posr < idx_posl:
                vstim_columns_tofix[idx_posr] ,vstim_columns_tofix[idx_posl] = vstim_columns_tofix[idx_posl], vstim_columns_tofix[idx_posr]
                vstim_interval.columns = vstim_columns_tofix

            for col in vstim_interval.columns:
                if col == 'code' or col == 'presentTime':
                    continue
                temp_df = vstim_interval[col]
                # if a column has all the same values, take the first entry of the column as the value
                # sf, tf, contrast, stim_side should run through here
                uniq = np.unique(temp_df)
                uniq = uniq[~np.isnan(uniq)]
                if len(uniq) == 1 and col != 'posx_r' and col != 'posx_l':
                    interval_dict[col] = temp_df.iloc[0]
                # if different values exist in the column, take it as a list
                # stim_pos runs through here 
                else:
                    interval_dict[col] = temp_df.tolist()

            # this is to make things easier later in the analysis, maybe not the best way
            interval_dict['contrast'] = interval_dict['contrast_r'] if interval_dict['correct'] else interval_dict['contrast_l']
            interval_dict['stim_pos'] = interval_dict['posx_r'] if interval_dict['correct'] else interval_dict['posx_l']
            interval_dict['spatial_freq'] = interval_dict['sf_r'] if interval_dict['correct'] else interval_dict['sf_l']
            interval_dict['temporal_freq'] = interval_dict['tf_r'] if interval_dict['correct'] else interval_dict['tf_l']
            interval_dict['stim_side'] = np.sign(interval_dict['stim_pos'][0])

        wheel = self.data['position']
        wheel_interval = wheel[(wheel['duinotime'] >= start) & (wheel['duinotime'] <= end)].dropna()
        
        # add wheel
        interval_dict['wheel'] = np.asarray(wheel_interval[['duinotime', 'value']]).tolist()
        
        # add lick
        lick_amount = 0.8 #uL
        try:
            lick = self.data['lick']
            lick_interval = lick[(lick['duinotime'] >= start) & (lick['duinotime'] <= end)].dropna()

            lick_count = len(np.asarray(lick_interval[['duinotime', 'value']]).tolist())
            
            if lick_count:
                interval_dict['lick_count'] = lick_count
            else:
                interval_dict['lick_count'] = 0
        except:
            interval_dict['lick_count'] = 0

        # add reward click
        # the interval needs to be extended to get the click in state 4(lasts 2 seconds)
        reward = self.data['reward']
        reward_amount = 3 #uL
        reward_interval = reward[(reward['duinotime'] >= start) & (reward['duinotime'] <= end + 2000)].dropna()
        interval_dict['reward'] = reward_amount * len(np.asarray(reward_interval[['duinotime', 'value']]).tolist())

        #add opto
        try:
            opto = self.data['opto']
            opto_interval = opto[(opto['duinotime'] >= start) & (opto['duinotime'] <= end)].dropna()
            if len(opto_interval) != 0:
                # no opto stim in this trial
                interval_dict['opto'] = 1
            else:
                interval_dict['opto'] = 0
        except:
            interval_dict['opto'] = 0
        
        return interval_dict
              
    def get_trial_data(self,trial_slice):
        """ Iterates over each state change in a DataFrame slice that belongs to one trial
            :param trial_slice: pandas DataFrame object
            :type trial_slice: DataFrame
            :return: A list of dictionaries that are trials(first and later corrections)
            :rtype: list
        """
        temp_row = {'trial_no': int(trial_slice[self.column_keys['trialNo']].iloc[0])}
        trial_rows = []
        firstanswer_flag = True
        for index, row in trial_slice.iterrows():
            curr_key = '{0}->{1}'.format(int(row[self.column_keys['oldState']]), int(row[self.column_keys['newState']]))

            # stim start
            if curr_key == self.transition_keys['openloopstart']:
                temp_row['openloopstart'] = row[self.column_keys['elapsed']]
                temp_row['correction'] = row[self.column_keys['trialType']]

            # move start
            elif curr_key == self.transition_keys['closedloopstart']:
                temp_row['closedloopdur'] = [row[self.column_keys['elapsed']]]

            # correct
            elif curr_key == self.transition_keys['correct']:
                temp_row['response_latency'] = row[self.column_keys['stateElapsed']]
                temp_row['answer'] = 1
                temp_row['closedloopdur'].append(row[self.column_keys['elapsed']])

            # incorrect
            elif curr_key == self.transition_keys['incorrect']:
                temp_row['closedloopdur'].append(row[self.column_keys['elapsed']])
                temp_row['response_latency'] = row[self.column_keys['stateElapsed']]
                # stimpy has no seperate state for no answer (maybe should be added?) so it is extracted from stateElapsed time
                if self.logversion == 'stimpy':
                    # longer than given time to answer so a no answer
                    if temp_row['response_latency']>=float(self.opts['closedStimDuration'])*1000:
                        temp_row['answer'] = 0
                    else:
                        temp_row['answer'] = -1
                elif self.logversion == 'pyvstim':
                    temp_row['answer'] = -1

            # no answer(only enters in pyvstim log)
            elif curr_key == self.transition_keys['nonanswer']:
                temp_row['response_latency'] = row[self.column_keys['stateElapsed']]
                temp_row['answer'] = 0
                temp_row['closedloopdur'].append(row[self.column_keys['elapsed']])

            # correction or trial end
            elif curr_key == self.transition_keys['correction'] or curr_key == self.transition_keys['trialend']:
                if self.logversion == 'pyvstim':
                    if firstanswer_flag:
                        temp_row['correction'] = 0
                        firstanswer_flag = False
                    else:
                        if 'correction' in temp_row.keys():
                            temp_row['correction'] += 1
                        else:
                            temp_row['correction'] = 1  


                interval_row = self.add_data_interval(temp_row['openloopstart'],temp_row['closedloopdur'][1])
                temp_row = {**temp_row, **interval_row}

                trial_rows.append(temp_row)
        return trial_rows
        
    def get_session_data(self):
        """ The main loop where the parsed session data is created
            :return: Parsed sessiondata
            :rtype:  DataFrame
        """
        session_data = pd.DataFrame()
        self.states = self.data['stateMachine']
        
        if self.states.shape[0] is 0:
            display('No state machine data to analyze, logging is problematic...')
            return None

        trials = np.unique(self.states[self.column_keys['trialNo']])
        # this is a failsafe for some early stimpy data where trial count has not been incremented
        if len(trials) == 1 and len(self.states) > 6 and self.logversion=='stimpy':
            self.extract_trial_count()
            trials = np.unique(self.states[self.column_keys['trialNo']])

        for t in trials:
            # print(t)
            trial_slice = self.states[self.states[self.column_keys['trialNo']] == t]
            trial_rows = self.get_trial_data(trial_slice)

            if len(trial_rows):
                session_data = session_data.append(trial_rows, ignore_index=True)
            else:
                print(t)
                print('OH NO!!!')

        if session_data.empty:
            print('''WARNING THIS SESSION HAS NO DATA
            Possible causes:
            - Session has only one trial with no correct answer''')
            return None
        else:
            return session_data

    def get_rolling_stats(self,data_in, rolling_size=10):
        """ Gets the rolling stats (accuracy and resonse time) for a given slice of data
            :param data_in: input data, mostly the DataFrame for a specific stimuli(e.g. highSF_lowTF_opto)
            :param rolling_size: size of rolling average window
            :type data_in: DataFrame 
            :type rolling_size: int
        """
        data_in['accuracy'] = 0
        temp_correct = 0.0
        for i,row in data_in.iterrows():
            if row['answer'] == 1:
                temp_correct += 1
            data_in.loc[i,'accuracy'] = temp_correct/row.trial_no

        # get rolling averages
        data_in['running_latency'] = data_in['response_latency'].rolling(rolling_size).mean()
        data_in['running_accuracy'] = data_in['accuracy'].rolling(rolling_size).mean()

        return data_in

    # TODO : GET BACK TO THIS, STILL IN BETA
    def get_wheel_response(self,mode='pos',thresh=200,adapt=False):
        # Gets the response time from wheel movement
        # mode = 'time' => Registers the trial decision based on wheel movement in the threshold(ms) interval
        # mode = 'pos' => Registers the trial decision based on wheel

        # TODO add adaptive thresholding
        self.session_data.loc[:,'wheel_response_time'] = np.nan
        self.session_data.loc[:,'wheel_answer'] = np.nan
        if adapt:
            #TODO LOOK HERE FOR ALTERNATIVE THRESHOLDING
            thresh = median_threshold(self.session_data['wheel'])
            display('Adaptive Thresholding: {0} units'.format(thresh))

        for row in self.session_data.itertuples():
            
            iWheel = np.asarray(row.wheel)
            if len(iWheel)>2:
                
                move_direction = np.mean(np.gradient(iWheel[:,1]))
                start_t, start_loc = iWheel[0,:]
                
                if mode == 'time':
                    idx,val = find_nearest(iWheel[:,0],start_t + thresh)
                    thresh_t, thresh_loc = iWheel[idx,:]
                    if move_direction > 0:          
                    #moving right
                        if row.stim_side == 1:   
                        #stim right
                            self.session_data.loc[row.Index,'wheel_answer'] = -1
                        else:
                        #stim left
                            self.session_data.loc[row.Index,'wheel_answer'] = 1
                    elif move_direction < 0:
                    #moving left
                        if row.stim_side == 1:   
                        #stim right
                            self.session_data.loc[row.Index,'wheel_answer'] = 1
                        else:
                        #stim left
                            self.session_data.loc[row.Index,'wheel_answer'] = -1
                    else:
                    #no movement?
                        self.session_data.loc[row.Index,'wheel_answer'] = 0


                    self.session_data.loc[row.Index,'wheel_response_time'] = val
                else:
                    if move_direction > 0:
                    #move right
                        idx,val = find_nearest(iWheel[:,1],start_loc+thresh)
                        thresh_t, thresh_loc = iWheel[idx,:]
                        if row.stim_side == 1:
                        #stim right
                            self.session_data.loc[row.Index,'wheel_answer'] = -1
                        else:
                        #stim left
                            self.session_data.loc[row.Index,'wheel_answer'] = 1  
                        self.session_data.loc[row.Index,'wheel_response_time'] = thresh_t - start_t

                    elif move_direction <0:
                    #move left
                        idx,val = find_nearest(iWheel[:,1],start_loc-thresh)
                        thresh_t, thresh_loc = iWheel[idx,:]
                        if row.stim_side == 1:
                        #stim right
                            self.session_data.loc[row.Index,'wheel_answer'] = 1
                        else:
                        #stim left
                            self.session_data.loc[row.Index,'wheel_answer'] = -1
                        self.session_data.loc[row.Index,'wheel_response_time'] = thresh_t - start_t
                    else:
                    #no movement?
                        self.session_data.loc[row.Index,'wheel_answer'] = 0
                        #??
                        self.session_data.loc[row.Index, 'wheel_response_time'] = 0   
            else:
            # no actual direction in the threshold interval
                self.session_data.loc[row.Index,'wheel_answer'] = 0

    def get_summary(self,data_in):
        """ Summarizes the percentages, latency and stim side profiles
            :param data_in: input data, mostly the DataFrame for a specific stimuli(e.g. highSF_lowTF_opto)
            :type data_in: DataFrame
        """
        all_correct_pct = 100 * len(data_in[data_in['answer']==1])/len(data_in)

        novel_data = data_in[data_in['correction']==0]

        #left
        left_data = novel_data[novel_data['stim_side'] == -1]
        left_profile = [-len(left_data[left_data['answer']==1]), -len(left_data[left_data['answer']==-1]),-len(left_data[left_data['answer']==0])]
        #right 
        right_data = novel_data[novel_data['stim_side'] == 1]
        right_profile = [len(right_data[right_data['answer']==1]), len(right_data[right_data['answer']==-1]),len(right_data[right_data['answer']==0])]

        answered_data = novel_data[novel_data['answer']!=0]
        
        novel_correct_pct = 100 * len(answered_data[answered_data['answer']==1])/len(novel_data)
        answered_correct_pct = 100 * len(answered_data[answered_data['answer']==1])/len(answered_data)

        total_latency = np.sum(answered_data['response_latency'])

        return {'all_trials' : len(data_in),
                'novel_trials' : len(novel_data),
                'answered_trials' : len(answered_data),
                'all_correct_pct' : all_correct_pct,
                'novel_correct_pct': novel_correct_pct,
                'answered_correct_pct' : answered_correct_pct,
                'left_profile' : left_profile,
                'right_profile' : right_profile,
                'latency' : total_latency / len(answered_data)}

    def analyze_session(self):
        """ Analyzes the session by dividing into chunks depending on different stim poperties(sf and tf) and opto/nonopto
            This consists of summarizing, calculating the psychometric curve and trajectories
            It also produces the novel_data used for the analysis"""
        start = time.time()
        self.summaries = {}
        self.curves = {}
        self.trajectories = {}
        self.novel_stim_data  = {}

        # summary of the overall data, without seperating stim types and opto
        self.summaries['overall'] = self.get_summary(self.session_data)
        self.novel_stim_data['overall'] = self.get_rolling_stats(self.session_data[self.session_data['correction']==0])

        sfreq = np.unique(self.session_data['spatial_freq'])
        tfreq = np.unique(self.session_data['temporal_freq'])

        # analysing each stim type and opto seperately
        for i in enumerate(sfreq):
            if sfreq[i] == 0.4 and tfreq[i] == 0.5:
                key = 'highSF_lowTF'
            elif sfreq[i] == 0.4 and tfreq[i] == 16:
                key = 'highSF_highTF'
            elif sfreq[i] == 0.05 and tfreq[i] == 0.5:
                key = 'lowSF_lowTF'
            elif sfreq[i] == 0.05 and tfreq[i] == 16:
                key = 'lowSF_highTF'
            else:
                key = 'grating'

            if self.session_meta['opto']:
                key += '_opto'

            display("Analysing {0} ...".format(key))
            
            # stimuli data
            stimuli_data = self.session_data[(self.session_data['spatial_freq'] == sfreq[i]) &          # spatial freq filter
                                         (self.session_data['temporal_freq'] == tfreq[i]) &         # temporal freq filter
                                         (self.session_data['opto'] == self.session_meta['opto'])]  # opto filter
            
            # summaries
            self.summaries[key] = self.get_summary(stimuli_data)

            stimuli_data_novel = stimuli_data[stimuli_data['correction'] == 0]

            # analysis for psychometric curve
            self.curves[key] = WheelCurve(name=key, rawdata=stimuli_data_novel)

            # analysis for trajectories
            sides = np.unique(stimuli_data_novel['stim_side'])

            for s in sides:
                if s <= 0:
                    sname = '_left'
                elif s > 0:
                    sname = '_right'
                key += sname
                side_stimuli_data_novel = stimuli_data_novel[stimuli_data_novel['stim_side'] == s]
                self.trajectories[key] = WheelTrajectory(name=key, rawdata=side_stimuli_data_novel)

            # data frame data f trials
            self.novel_stim_data[key] = self.get_rolling_stats(stimuli_data_novel)

        end = time.time()
        display("Done t={0} seconds".format(end-start))

    def extract_trial_count(self):
        """ Extracts the trial no from state changes, this works for stimpy for now"""
        display('Trial increment faulty, extracting from state changes...')
        trial_end_list = self.states.index[self.states['oldState']==6].tolist()
        temp_start = 0
        for i,end in enumerate(trial_end_list,1):
            self.states.loc[temp_start:end,'cycle'] = i
            temp_start = end + 1

    def plot(self,plotkey,ax=None,savefig=True,closefig=False,*args,**kwargs):
        
        plt_title = '{0} {1}'.format(self.date,self.animalid)


        if plotkey == 'psychometric':
            fig_main = plt.figure(figsize=kwargs.get('figsize',(15,15)))
            ax = fig_main.add_subplot(1, 1, 1)
            ax = plotPsychometric(self.session_summary['curves'],plt_title,ax=ax,*args,**kwargs)
        
        elif plotkey == 'trajectory':
            fig_main = plt.figure(figsize=kwargs.get('figsize',(20,10)))
            nrow = np.ceil(len(self.session_summary['trajectories'].keys()))
            ncol = 1 if nrow==1 else 2
            for i,key in enumerate(self.session_summary['trajectories'].keys(),1):
                ax = fig_main.add_subplot(nrow, ncol, i)
                ax = plotTrajectories(self.session_summary['trajectories'][key],s_limit=2000,title=plt_title,ax=ax,*args,**kwargs)
        
        elif plotkey == 'trialprogress':
            fig_main = plt.figure(figsize=kwargs.get('figsize',(18,9)))
            ax = fig_main.add_subplot(1, 1, 1)
            ax = plotTrialProgress(self.session_data,plt_title,ax=ax,*args,**kwargs)
        
        elif plotkey == 'performance':
            fig_main = plt.figure(figsize=kwargs.get('figsize',(15,15)))
            ax = fig_main.add_subplot(1, 1, 1)
            ax = plotPerformance(self.session_data_novel,plt_title,ax=ax,*args,**kwargs)
        
        elif plotkey == 'stimside':
            fig_main = plt.figure(figsize=kwargs.get('figsize',(15,15)))
            ax = fig_main.add_subplot(1, 1, 1)
            ax = plotResponseTime(self.session_data_novel,plt_title,ax=ax,*args,**kwargs)

        fig_main.axes.append(ax)
        if savefig:
            figsave_loc = pjoin(self.savepath,'sessionFigures')
            if not os.path.exists(figsave_loc):
                os.mkdir(figsave_loc)

            savename = '{0}_{1}_{2}.pdf'.format(self.experimentname.split('_')[0], self.animalid,plotkey)
            savepath = pjoin(figsave_loc, savename)
            fig_main.savefig(savepath,bbox_inches='tight')
            display('{0} plot saved in {1}'.format(plotkey,self.savepath))

        if closefig:
            plt.close(fig_main)

    def plot_all(self):
        keys = ['psychometric','trajectory','trialprogress','performance','stimside']
        for k in keys:
            self.plot(k,fontsize=23,closefig=self.autoplot)

def main():
    # WheelParser('200325_KC020_opto_wheel_KC',load_flag=False,spit_mat=True)
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Wheel Behavior Data Parsing Tool')

    parser.add_argument('expname',metavar='expname',
                        type=str,help='Experiment filename (e.g. 200325_KC020_wheel_KC)')
    parser.add_argument('-l','--load',metavar='load_flag',default=True,
                        type=str,help='Flag for loading existing data')
    parser.add_argument('-m','--mat',metavar='spit_mat',
                        type=str,help='Flag for outputting a .mat file too',default=False)
    # print(os.path.realpath(__file__))
    opts = parser.parse_args()
    expname = opts.expname
    load_flag = opts.load
    spit_mat = opts.mat

    display('Parsing wheel data...')
    w = WheelParser(sessiondir = expname, load_flag=load_flag, spit_mat=spit_mat)

if __name__ == '__main__':
    main()

