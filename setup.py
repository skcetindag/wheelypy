#!/usr/bin/env python
# S. Kaan Cetindag - June 2020
from setuptools import setup
from setuptools.command.install import install


setup(
    name='wheelypy',
    version='1.0',
    author='S. Kaan Cetindag',
    author_email='cetindag.kaan@gmail.com',
    packages=['wheelypy'],
    license='Creative Commons Attribution-Noncommercial-Share Alike license',
    long_description='Behavior Analysis Tool\n Disclaimer: For now works for wheel behaviour, more to come...',
    entry_points = {
        'console_scripts': [
          'wheel = wheelypy.wheelExperiment:main',
          'wheelbehave = wheelypy.wheelBehavior:main'
        ]
        },
)